# Random Grid

Create grids for use in painted panels.

Inputs:
- number of samples
- number of reptitions
- number of columns
- force first sample (first sample will be in a random position on each row)

Outputs:
- onscreen grid
- export to csv


### Development

Development server: http://localhost:8080
```
npm install
npm start

```

Build files for hosting:

```
npm install
npm run build
```
