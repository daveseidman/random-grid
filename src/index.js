import autoBind from 'auto-bind';
import onChange from 'on-change';
import { addEl, createEl } from 'lmnt';
import { shuffle, containsAdjacents } from './utils';

import './index.scss';

class App {
  constructor() {
    autoBind(this);
    const state = {
      samples: 20,
      repetitions: 3,
      columns: 6,
      control: 1,
      forceControl: false,
      array: [],
    };

    this.state = onChange(state, this.update, { ignoreKeys: ['array'] });
    this.el = createEl('div', { className: 'app' });

    this.settingsContainer = createEl('div', { className: 'settings' });
    this.samplesLabel = createEl('label', { innerText: 'Samples:' });
    this.samplesInput = createEl('input', { type: 'Number', value: this.state.samples }, {}, { input: ({ target }) => { this.state.samples = parseInt(target.value, 10); } });
    this.repetitionsLabel = createEl('label', { innerText: 'Repetitions:' });
    this.repetitionsInput = createEl('input', { type: 'Number', value: this.state.repetitions }, {}, { input: ({ target }) => { this.state.repetitions = parseInt(target.value, 10); } });
    this.columnsLabel = createEl('label', { innerText: 'Columns:' });
    this.columnsInput = createEl('input', { type: 'Number', value: this.state.columns }, {}, { input: ({ target }) => { this.state.columns = parseInt(target.value, 10); } });
    this.forceControlLabel = createEl('label', { innerText: 'Force control per row' });
    this.forceControlInput = createEl('input', { type: 'checkbox', checked: this.state.forceControl }, {}, { input: ({ target }) => { this.state.forceControl = target.checked; } });
    addEl(this.settingsContainer, this.samplesLabel, this.samplesInput, this.repetitionsLabel, this.repetitionsInput, this.columnsLabel, this.columnsInput, this.forceControlLabel, this.forceControlInput);
    this.createGridButton = createEl('button', { innerText: 'create grid' }, {}, { click: this.createGrid });
    this.saveGridButton = createEl('button', { innerText: 'save grid', className: 'disabled' }, {}, { click: this.saveGrid });
    this.gridContainer = createEl('div', { className: 'grid' });
    addEl(this.el, this.settingsContainer, this.createGridButton, this.saveGridButton, this.gridContainer);
  }

  update(path, current, previous) {
    console.log(path, previous, '->', current);
  }

  createGrid() {
    // empty the array
    this.state.array = [];
    let count = 0;
    // fill it with each sample number, repeated the correct amount of times
    for (let i = 0; i < this.state.samples; i += 1) {
      count += 1;
      for (let j = 0; j < this.state.repetitions; j += 1) {
        this.state.array.push(count);
      }
    }
    // shuffle the entire array
    while (containsAdjacents(this.state.array, this.state.columns)) {
      this.state.array = shuffle(this.state.array);
    }

    // if forcing control sample on each row
    if (this.state.forceControl) {
      const leftovers = [];
      let newArray = [];
      let row = [];
      // loop through each row
      for (let i = 0; i < this.state.array.length / this.state.columns; i += 1) {
        const start = i * this.state.columns;
        const end = start + this.state.columns;
        row = this.state.array.slice(start, end);
        // row does not contain control, add it and
        if (row.indexOf(this.state.control) === -1) {
          leftovers.push(row.pop());
          row.push(this.state.control);
          // shuffle so that control isn't at the end
          row = shuffle(row);
        }
        // and shuffle again in case there are adjacents
        while (containsAdjacents(row, this.state.columns)) {
          row = shuffle(row);
        }
        newArray = newArray.concat(row);
      }

      // take all the items removed to make room for the controls
      let leftoversWithForced = [];
      while (leftovers.length >= this.state.columns) {
        let newRow = leftovers.splice(0, Math.min(this.state.columns - 1, leftovers.length));
        newRow.push(this.state.control);
        newRow = shuffle(newRow);
        leftoversWithForced = leftoversWithForced.concat(newRow);
      }
      newArray = newArray.concat(leftoversWithForced);
      // console.log(leftovers);
      this.state.array = newArray;
    }

    this.gridContainer.innerHTML = '';
    for (let i = 0; i < this.state.array.length; i += 1) {
      const item = createEl('span', { className: `grid-item ${this.state.array[i] === this.state.control ? 'control' : ''}`, innerText: this.state.array[i] }, {}, {
        pointerover: ({ target }) => {
          Array.from(this.gridContainer.children).forEach((el) => {
            el.classList[el.innerText === target.innerText ? 'add' : 'remove']('highlight');
          });
        },
        pointerout: () => {
          Array.from(this.gridContainer.children).forEach((el) => {
            el.classList.remove('highlight');
          });
        } });
      addEl(this.gridContainer, item);
      if ((i + 1) % this.state.columns === 0) {
        const lineBreak = createEl('span', { className: 'grid-break' });
        addEl(this.gridContainer, lineBreak);
      }
    }

    this.saveGridButton.classList.remove('disabled');
  }

  saveGrid() {
    const rows = [];
    const headers = [];
    for (let i = -1; i < this.state.columns; i += 1) {
      headers.push(i >= 0 ? i + 1 : '');
    }
    rows.push(headers);
    for (let i = 0; i < this.state.array.length / this.state.columns; i += 1) {
      const start = i * this.state.columns;
      const end = start + this.state.columns;
      const row = [`P${i + 1}`];
      // row = row.concat()
      rows.push(row.concat(this.state.array.slice(start, end)));
    }

    const csvContent = `data:text/csv;charset=utf-8,${rows.map(e => e.join(',')).join('\n')}`;

    const encodedUri = encodeURI(csvContent);
    const link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `random-grid_s=${this.state.samples}_r=${this.state.repetitions}_c=${this.state.columns}.csv`);
    document.body.appendChild(link);
    link.click();
  }
}


const app = new App();
if (location.hostname === 'localhost') window.app = app;
addEl(app.el);
